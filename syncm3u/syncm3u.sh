#!/bin/bash

#This script is used to copy songs read from an m3u file
# and to copy them in a folder
#
# Author: Andrea Beconcini

function help {
	echo -e "Usage:\n\t$0 <m3ufile> <destination>\n"
}

count=0

#checking for arguments
if [[ $# -lt 2 ]] ; then
	echo "No arguments supplied"
	help
	exit 1
fi

while read -r ; do
	#Skipping comments
	echo $REPLY | grep -e '#' &>/dev/null && continue

	file="$(echo $REPLY | sed 's/ /\\ /g' - | sed 's/'\''/\\'\''/g' - | sed 's/(/\\(/g' - | sed 's/)/\\)/g' - | sed 's/&/\\&/g' - )"
	#echo -e "$count : copying $file into $2\n"
	count=$(($count + 1))
	if [[ -f $REPLY ]] ; then
		cp "$REPLY" "$2"
	else
		echo "file" "$file" "not found";
	fi
done < "$1"

exit 0
