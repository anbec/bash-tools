#!/bin/bash

# This script make a compressed backup copy of a folder.
# The output archive name has prefix containing the date when the archive was created
#
# Author: Andrea Beconcini


stripped=`echo $1 | sed 's/\///'`
filename="`date +%Y-%m-%d_%H%M%S`_$stripped.tar.gz"
#echo $filename
tar -czf ${filename} "$1"
